#! /usr/bin/env sh

# Strict shell
set -o errexit
set -o nounset

# Substitutions
CP="{{cp}}"
CHMOD="{{chmod}}"
MKDIR="{{mkdir}}"
DESTINATION="{{destination}}"
DATA="{{data}}"
readonly CP CHMOD MKDIR DESTINATION DATA

printf '%s\n' "${DATA}" |
  while IFS= read -r FILEPATH; do
    DEST="${DESTINATION}"
    while true; do
      case "${DEST}" in
      *"{{root}}"*)
        DEST="${DEST%%\{\{root\}\}*}${BUILD_WORKSPACE_DIRECTORY?Run script with Bazel}${DEST#*\{\{root\}\}}"
        ;;
      *"{{cwd}}"*)
        DEST="${DEST%%\{\{cwd\}\}*}${BUILD_WORKING_DIRECTORY?Run script with Bazel}${DEST#*\{\{cwd\}\}}"
        ;;
      *"{{dirname}}"*)
        DEST="${DEST%%\{\{dirname\}\}*}${FILEPATH%/*}${DEST#*\{\{dirname\}\}}"
        ;;
      *"{{basename}}"*)
        DEST="${DEST%%\{\{basename\}\}*}${FILEPATH##*/}${DEST#*\{\{basename\}\}}"
        ;;
      *"{{"[!\}]*"}}"*)
        DEST="${DEST#*\{\{}"
        DEST="${DEST%%\}\}*}"
        printf >&2 'Invalid destination replacement: {{%s}}\n' "${DEST}"
        exit 1
        ;;
      *)
        break
        ;;
      esac
    done
    RUNFILE="${FILEPATH}"
    if ! test -f "${RUNFILE}"; then
      printf >&2 'Failed to find runfile: %s\n' "${RUNFILE}"
    fi
    "${MKDIR}" -p "${DEST%/*}"
    printf >&2 '%s -> %s\n' "${FILEPATH}" "${DEST}"
    "${CP}" "${RUNFILE}" "${DEST}"
    "${CHMOD}" u+w "${DEST}"
  done
