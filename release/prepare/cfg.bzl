load("@bazel_skylib//lib:structs.bzl", "structs")

visibility("//...")

def implementation(_, attr):
    map = structs.to_dict(attr)
    return {"//command_line_option:compilation_mode": map.get("compilation_mode", "opt")}

release_prepare_cfg = transition(
    implementation = implementation,
    inputs = [],
    outputs = ["//command_line_option:compilation_mode"],
)

cfg = release_prepare_cfg
