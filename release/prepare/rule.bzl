load(":cfg.bzl", _cfg = "cfg")

visibility("//...")

DOC = """Prepares a release.

Copies provided `data` targets to a destination directory.
"""

ATTRS = {
    "data": attr.label_list(
        doc = "The targets to copy to the destination directory",
        allow_empty = False,
        mandatory = True,
    ),
    "destination": attr.string(
        doc = "A destination directory template.",
        default = "{{bwd}}/dest/{{dirname}}/{{basename}}",
    ),
    "_template": attr.label(
        default = ":template",
        allow_single_file = True,
    ),
}

def _short_path(file):
    return file.short_path

def implementation(ctx):
    cp = ctx.toolchains["@rules_coreutils//coreutils/toolchain/cp:type"]
    chmod = ctx.toolchains["@rules_coreutils//coreutils/toolchain/chmod:type"]
    mkdir = ctx.toolchains["@rules_coreutils//coreutils/toolchain/mkdir:type"]

    rendered = ctx.actions.declare_file("{}.{}".format(ctx.label.name, ctx.file._template.extension))

    substitutions = ctx.actions.template_dict()
    substitutions.add("{{cp}}", cp.executable.short_path)
    substitutions.add("{{chmod}}", chmod.executable.short_path)
    substitutions.add("{{mkdir}}", mkdir.executable.short_path)
    substitutions.add("{{destination}}", ctx.attr.destination)
    substitutions.add_joined("{{data}}", depset(ctx.files.data), map_each = _short_path, join_with = "\n")

    ctx.actions.expand_template(
        template = ctx.file._template,
        output = rendered,
        is_executable = True,
        computed_substitutions = substitutions,
    )

    runfiles = ctx.runfiles(files = ctx.files.data)
    runfiles = runfiles.merge(cp.default.default_runfiles)
    runfiles = runfiles.merge(chmod.default.default_runfiles)
    runfiles = runfiles.merge(mkdir.default.default_runfiles)

    return DefaultInfo(
        executable = rendered,
        files = depset([rendered]),
        runfiles = runfiles,
    )

release_prepare = rule(
    attrs = ATTRS,
    doc = DOC,
    cfg = _cfg,
    implementation = implementation,
    toolchains = [
        "@rules_coreutils//coreutils/toolchain/cp:type",
        "@rules_coreutils//coreutils/toolchain/chmod:type",
        "@rules_coreutils//coreutils/toolchain/mkdir:type",
    ],
    executable = True,
)

prepare = release_prepare
