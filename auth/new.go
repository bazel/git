package auth

import (
	"net/url"
	"os"

	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/go-git/go-git/v5/plumbing/transport/ssh"
)

func New(remote url.URL) (transport.AuthMethod, error) {
	switch remote.Scheme {
	case "ssh":
		username := ""
		if remote.User != nil {
			username = remote.User.Username()
		}

		if _, ok := os.LookupEnv("SSH_AUTH_SOCK"); ok {
			return ssh.NewSSHAgentAuth(username)
		}

		return newKeys(username)
	}

	return nil, nil
}
