package auth

import (
	"fmt"
	"log/slog"
	"os"
	u "os/user"
	"path"
	"strings"

	transport "github.com/go-git/go-git/v5/plumbing/transport/ssh"
	"golang.org/x/crypto/ssh"
)

type keys struct {
	User    string
	signers []ssh.Signer
	transport.HostKeyCallbackHelper
}

func newKeys(user string) (transport.AuthMethod, error) {
	log := slog.With("method", "auth.signers")

	var a keys
	if user != "" {
		a.User = user
	} else if user, err := u.Current(); err != nil {
		a.User = user.Username
	} else if user, ok := os.LookupEnv("USER"); ok {
		a.User = user
	} else {
		return nil, fmt.Errorf("failed to get username for `%s`", a.Name())
	}

	home, err := os.UserHomeDir()
	if err != nil {
		return nil, err
	}

	root := path.Join(home, ".ssh")
	files, err := os.ReadDir(root)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		prefix, suffix, found := strings.Cut(file.Name(), "_")
		if found && prefix == "id" && !strings.HasSuffix(suffix, ".pub") && !file.IsDir() {
			filepath := path.Join(root, file.Name())
			bytes, err := os.ReadFile(filepath)
			if err != nil {
				return nil, err
			}

			signer, err := ssh.ParsePrivateKey(bytes)

			if _, ok := err.(*ssh.PassphraseMissingError); ok {
				// TODO: possibly support `SSK_ASKPASS` to read the password?
				log.Warn("SSH key with passphrase unsupported, skipping. Use `ssh-agent` for passphrase protected keys. `SSH_AUTH_SOCK` must be available for `ssh-agent` connections.", "filepath", filepath)
				continue
			}
			if err != nil {
				return nil, err
			}

			log.Debug("Unlocked SSH key", "filepath", filepath)
			a.signers = append(a.signers, signer)
		}
	}

	if len(a.signers) == 0 {
		return nil, fmt.Errorf("no password-less ssh keys found: %s", root)
	}

	return a, nil
}

func (_ keys) Name() string {
	return "ssh-public-keys"
}

func (a keys) String() string {
	return fmt.Sprintf("user: %s, name: %s", a.User, a.Name())
}

func (a keys) ClientConfig() (*ssh.ClientConfig, error) {
	return a.SetHostKeyCallback(&ssh.ClientConfig{
		User: a.User,
		Auth: []ssh.AuthMethod{ssh.PublicKeys(a.signers...)},
	})
}
