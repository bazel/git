package remote

import (
	"net/url"
	"strings"

	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/kevinburke/ssh_config"
	"gitlab.arm.com/bazel/git/v1/auth"
)

type Flag struct {
	u *url.URL
	a transport.AuthMethod
}

func NewFlag() Flag {
	return Flag{}
}

func (f Flag) URL() url.URL {
	return *f.u
}

func (f Flag) Auth() transport.AuthMethod {
	return f.a
}

func (f *Flag) UnmarshalFlag(value string) error {
	// TODO: support remote names

	// Convert `rsync` style to `ssh://`
	host, path, found := strings.Cut(value, ":")
	if found && !strings.HasPrefix(path, "//") {
		value = "ssh://" + host + "/" + path
	}

	parsed, err := url.Parse(value)
	if err != nil {
		return err
	}

	if parsed.Scheme == "ssh" && parsed.User == nil {
		user, err := ssh_config.GetStrict(parsed.Hostname(), "User")
		if err != nil {
			return err
		}
		if user != "" {
			parsed.User = url.User(user)
		}
	}

	auth, err := auth.New(*parsed)
	if err != nil {
		return err
	}

	f.u = parsed
	f.a = auth

	return nil
}

func (f Flag) String() string {
	return f.u.String()
}
