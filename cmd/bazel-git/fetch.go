package main

import (
	"bytes"
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"strings"

	"github.com/bmatcuk/doublestar/v4"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/protocol/packp/sideband"
	"github.com/jessevdk/go-flags"
	"github.com/koron-go/prefixw"
	"github.com/mattn/go-isatty"
	"github.com/natefinch/atomic"
	"gitlab.arm.com/bazel/git/v1/remote"
)

type RefSpec config.RefSpec

func (r *RefSpec) UnmarshalFlag(value string) error {
	if strings.Contains(value, ":") {
		// passthrough
	} else if plumbing.IsHash(value) {
		value = value + ":" + value
	} else if !strings.HasPrefix(value, "refs/") {
		value = "refs/heads/" + value + ":/refs/heads/" + value
	} else {
		value = value + ":" + value
	}
	refspec := config.RefSpec(value)
	*r = RefSpec(refspec)
	if err := refspec.Validate(); err != nil {
		return fmt.Errorf("invalid refspec '%s'", value)
	}
	return nil
}

// TODO: upstream this to `go-git`
type Reference struct {
	plumbing.Reference
}

type FetchStatus int

const (
	FetchStatusNotForMerge FetchStatus = 0
	FetchStatusMerge       FetchStatus = 1
)

func (r Reference) FetchHead(status FetchStatus, urls []string) string {
	name := r.Name()

	message := r.Hash().String()

	switch status {
	case FetchStatusNotForMerge:
		message += "\tnot-for-merge"
	default:
		message += "\t"
	}

	switch {
	case name.String() == "HEAD":
		message += "\t"
	case name.IsBranch():
		message += fmt.Sprintf("\tbranch '%s' of ", name.Short())
	case name.IsTag():
		message += fmt.Sprintf("\ttag '%s' of ", name.Short())
	case name.IsRemote():
		message += fmt.Sprintf("\tremote-tracking branch '%s' of ", name.Short())
	default:
		message += fmt.Sprintf("\t'%s' of ", name.Short())
	}

	message += strings.Join(urls, "\\n")

	return message
}

type FetchCommand struct {
	Depth            int                 `long:"depth" value-name:"<depth>" description:"Limit fetching the specified number of commits from the tip of each remote branch history." require:"yes"`
	NoWriteFetchHead bool                `long:"no-write-fetch-head" description:"Does not write **FETCH_HEAD**."`
	NegotiationTips  []plumbing.Revision `long:"negotiation-tip" value-name:"<commit>" description:"Determines the tips to use in negoatiating a smaller packfile transfer. Can be defined multiple times."`
	Args             struct {
		Remote   remote.Flag `positional-arg-name:"repository" description:"The remote to retrieve objects from."`
		RefSpecs []RefSpec   `positional-arg-name:"refspec" description:"The committish to retrieve from the server. Usually a commit SHA but can be a resolvable reference."`
	} `positional-args:"yes" required:"yes"`
}

var fetchCommand FetchCommand

func (x *FetchCommand) Execute(rest []string) error {
	if len(rest) != 0 {
		return &flags.Error{flags.ErrDuplicatedFlag, "invalid number of positional arguments"}
	}

	log := slog.With("cmd", "fetch", "remote", x.Args.Remote, "refspecs", x.Args.RefSpecs)

	log.Debug("open", "git-dir", options.GitDir)
	repo, err := git.PlainOpen(options.GitDir)
	if err != nil {
		log.Error("open", "err", err)
		return err
	}

	log.Debug("remote")
	r, err := repo.CreateRemoteAnonymous(&config.RemoteConfig{
		Name: "anonymous",
		URLs: []string{x.Args.Remote.String()},
	})
	if err != nil {
		log.Error("remote", "err", err)
		return err
	}

	refSpecs := make([]config.RefSpec, len(x.Args.RefSpecs))
	for i, refSpec := range x.Args.RefSpecs {
		refSpecs[i] = config.RefSpec(refSpec)
	}

	var progress sideband.Progress
	if isatty.IsTerminal(os.Stderr.Fd()) {
		// TODO: how to we print the progress for receiving objects/deltas?
		progress = prefixw.New(os.Stderr, "remote: ")
	}

	log.Debug("fetch")
	err = r.Fetch(&git.FetchOptions{
		RefSpecs: refSpecs,
		Depth:    x.Depth,
		Progress: progress,
		Auth:     x.Args.Remote.Auth(),
	})
	switch err {
	case git.ErrExactSHA1NotSupported:
		wants := make([]string, len(refSpecs))
		for i, refSpec := range refSpecs {
			wants[i] = refSpec.Src()
		}
		return NewExitCodeError(128, fmt.Sprintf("remote error: want %s not valid", strings.Join(wants, ",")))
	default:
		log.Error("fetch", "err", err)
		return err
	case git.NoErrAlreadyUpToDate:
	case nil:
	}

	if !x.NoWriteFetchHead {
		log.Debug("FETCH_HEAD")

		iter, err := repo.References()
		if err != nil {
			log.Error("FETCH_HEAD", "err", err)
			return err
		}

		buffer := bytes.NewBuffer([]byte{})
		urls := []string{x.Args.Remote.String()}

		err = iter.ForEach(func(reference *plumbing.Reference) error {
			for _, refSpec := range refSpecs {
				// TODO: upstream a local reference match to `go-git`
				// Needs to match on `refSpec.Dst()` not `refSpec.Src()` which `refspec.Match()` does
				pattern := strings.Split(refSpec.String(), ":")[1]
				match, err := doublestar.Match(pattern, reference.Name().String())
				if err != nil {
					return err
				}
				if match {
					ref := Reference{*reference}
					line := ref.FetchHead(FetchStatusMerge, urls)
					_, err := buffer.WriteString(line + "\n")
					if err != nil {
						return err
					}
				}
			}

			return nil
		})
		if err != nil {
			log.Error("FETCH_HEAD", "err", err)
			return err
		}

		fetchHead := filepath.Join(options.GitDir, "FETCH_HEAD")
		err = atomic.WriteFile(fetchHead, buffer)
		if err != nil {
			return err
		}
	}

	return nil
}

func init() {
	parser.AddCommand("fetch",
		"Download objects from another repository.",
		`Fetches branches and/or tags (collectively, "refs") from a repositories, along with the objects necessary to complete their histories.`,
		&fetchCommand)
}
