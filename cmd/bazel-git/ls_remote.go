package main

import (
	"fmt"
	"log/slog"
	"strings"

	"github.com/bmatcuk/doublestar/v4"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/storage/memory"
	"github.com/jessevdk/go-flags"
	"gitlab.arm.com/bazel/git/v1/remote"
)

type LsRemoteCommand struct {
	ExitCode bool `long:"exit-code" description:"Exit with an error code when references cannot be resolved on the remote server."`
	Args     struct {
		Remote   remote.Flag `positional-arg-name:"<repository>" description:"The remote to resolve references against."`
		Patterns []string    `positional-arg-name:"<pattern>" description:"The commit or reference to resolve into a commit." required:"1"`
	} `positional-args:"yes" required:"yes"`
}

var lsRemoteCommand LsRemoteCommand

func (x *LsRemoteCommand) Execute(rest []string) error {
	if len(rest) != 0 {
		return &flags.Error{flags.ErrDuplicatedFlag, "invalid number of positional arguments"}
	}

	log := slog.With("cmd", "ls-remote", "url", x.Args.Remote, "patterns", x.Args.Patterns)

	log.Debug("remote")
	remote := git.NewRemote(memory.NewStorage(), &config.RemoteConfig{
		Name: "origin",
		URLs: []string{x.Args.Remote.String()},
	})

	// TODO: upstream support for sending `ls-remote` patterns
	log.Debug("listing")
	refs, err := remote.List(&git.ListOptions{
		Timeout: 60 * 5,
		Auth:    x.Args.Remote.Auth(),
	})
	if err != nil {
		return err
	}

	log.Debug("filtering")
	found := false
	for _, ref := range refs {
		for _, pattern := range x.Args.Patterns {
			matched, err := doublestar.Match(pattern, ref.Name().String())
			if err != nil {
				return err
			}
			if matched {
				fmt.Printf("%s\t%s\n", ref.Hash().String(), ref.Name())
				found = true
			}

			if strings.HasPrefix("/refs/", pattern) {
				continue
			}

			matched, err = doublestar.Match("refs/*/**/"+pattern, ref.Name().String())
			if err != nil {
				return err
			}
			if matched {
				fmt.Printf("%s\t%s\n", ref.Hash().String(), ref.Name())
				found = true
			}
		}
	}

	if !found && x.ExitCode {
		return NewExitCodeError(2, "")
	}

	return nil
}

func init() {
	parser.AddCommand("ls-remote",
		"List references in a remote repository.",
		`Displays references available in a remote repository along with the associated commit IDs.`,
		&lsRemoteCommand)
}
