package main

import (
	"errors"
)

type ExitCodeError struct {
	ExitCode int
	Err      error
}

func (e *ExitCodeError) Error() string {
	return e.Err.Error()
}

func NewExitCodeError(code int, msg string) error {
	return &ExitCodeError{code, errors.New(msg)}
}
