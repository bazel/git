visibility("//...")

COUPLETS = (
    ("amd64", "linux"),
    ("arm64", "linux"),
    ("amd64", "darwin"),
    ("arm64", "darwin"),
    ("amd64", "windows"),
    ("arm64", "windows"),
)
