package main

import (
	"errors"
	"log/slog"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/jessevdk/go-flags"
)

type CatFileCommand struct {
	Exists bool `short:"e" description:"Exit with zero status if <object> exists and is a valid object. If <object> is of an invalid format, exit with non-zero status and emit an error on stderr." required:"yes"`
	Args   struct {
		Revision plumbing.Revision `positional-arg-name:"<object>" description:"The object to check for existence."`
	} `positional-args:"yes" required:"yes"`
}

var catFileCommand CatFileCommand

func (x *CatFileCommand) Execute(rest []string) error {
	if len(rest) != 0 {
		return &flags.Error{flags.ErrDuplicatedFlag, "invalid number of positional arguments"}
	}

	log := slog.With("cmd", "cat-file", "object", x.Args.Revision)

	log.Debug("open", "git-dir", options.GitDir)
	repo, err := git.PlainOpen(options.GitDir)
	if err != nil {
		log.Error("open", "err", err)
		return err
	}

	log.Debug("resolve")
	hash, err := repo.ResolveRevision(x.Args.Revision)
	if x.Exists && err == plumbing.ErrReferenceNotFound {
		return NewExitCodeError(1, "")
	}
	if err != nil {
		log.Error("resolve", "err", err)
		return err
	}
	log.Debug("resolved", "hash", hash)

	if x.Exists && strings.Contains(x.Args.Revision.String(), ":") {
		log.Debug("commit")
		commit, err := repo.CommitObject(*hash)
		if err != nil {
			log.Error("commit", "err", err)
			return err
		}

		log.Debug("tree")
		tree, err := commit.Tree()
		if err != nil {
			log.Error("tree", "err", err)
			return err
		}

		filepath := strings.SplitN(x.Args.Revision.String(), ":", 2)[1]

		log.Debug("file", "filepath", filepath)
		_, err = tree.File(filepath)
		if err == object.ErrFileNotFound {
			return NewExitCodeError(1, "")
		}
		if err != nil {
			log.Error("tree", "err", err)
			return err
		}

		return nil
	}

	if x.Exists {
		return nil
	}

	// TODO: implement the rest of the `cat-file` functionality.
	return errors.New("Not implemented")
}

func init() {
	parser.AddCommand("cat-file",
		"Check for existence of an object.",
		`Detemines if a commit or tag object is availble in the repository.`,
		&catFileCommand)
}
