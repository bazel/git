package main

import (
	"encoding/hex"
	"fmt"
	"log/slog"
	"regexp"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/jessevdk/go-flags"
)

const gitLink = 0160000

type LsTreeCommand struct {
	Format string `long:"format" value-name:"<format>" description:"A string that interpolates %(fieldname) from the result being shown." default:"%(objectmode) %(objecttype) %(objectname)%x09%(path)"`
	Args   struct {
		Revision plumbing.Revision `positional-arg-name:"<tree-ish>" description:"Identifier or a tree-ish."`
		Paths    []string          `positional-arg-name:"<path>" description:"The paths to list."`
	} `positional-args:"yes" required:"yes"`
}

var lsTreeCommand LsTreeCommand

func (x *LsTreeCommand) Execute(rest []string) error {
	if len(rest) != 0 {
		return &flags.Error{flags.ErrDuplicatedFlag, "invalid number of positional arguments"}
	}

	log := slog.With("cmd", "ls-tree", "object", x.Args.Revision, "path", x.Args.Paths)

	log.Debug("open", "git-dir", options.GitDir)
	repo, err := git.PlainOpen(options.GitDir)
	if err != nil {
		log.Error("open", "err", err)
		return err
	}

	log.Debug("resolve")
	hash, err := repo.ResolveRevision(x.Args.Revision)
	if err != nil {
		log.Error("resolve", "err", err)
		return err
	}
	log.Debug("resolved", "hash", hash)

	log.Debug("commit")
	commit, err := repo.CommitObject(*hash)
	if err != nil {
		log.Error("commit", "err", err)
		return err
	}

	log.Debug("tree")
	tree, err := commit.Tree()
	if err != nil {
		log.Error("tree", "err", err)
		return err
	}

	entries := tree.Entries
	if 0 != len(x.Args.Paths) {
		entries = nil
		for _, path := range x.Args.Paths {
			log.Debug("find")
			entry, err := tree.FindEntry(path)
			if err != nil {
				log.Error("find", "err", err)
				return err
			}
			entry.Name = path
			entries = append(entries, *entry)
		}
	}

	re := regexp.MustCompile(`%(?:%|x[a-fA-F0-9]{2}|\((?:objectmode|objecttype|objectname|path)\))`)

	for _, entry := range entries {
		var kind string
		if entry.Mode == gitLink {
			kind = "commit"
		} else {
			object, err := repo.Object(plumbing.AnyObject, entry.Hash)
			if err != nil {
				return err
			}
			kind = object.Type().String()
		}

		replace := func(value string) string {
			switch value {
			case "%(objecttype)":
				return kind
			case "%(objectmode)":
				return fmt.Sprintf("%06o", entry.Mode)
			case "%(objectname)":
				return entry.Hash.String()
			case "%(path)":
				return entry.Name
			case "%%":
				return "%"
			}

			if strings.HasPrefix(value, "%x") {
				if bytes, err := hex.DecodeString(value[2:]); err == nil {
					return string(bytes)
				}
			}

			return value
		}

		line := re.ReplaceAllStringFunc(x.Format, replace)
		fmt.Println(line)
	}

	return err
}

func init() {
	parser.AddCommand("ls-tree",
		"List the contents of a tree object.",
		`Lists the contents of a given tree object, like what "/bin/ls -a" does in the current working directory.`,
		&lsTreeCommand)
}
