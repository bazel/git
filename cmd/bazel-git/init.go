package main

import (
	"fmt"
	"log/slog"

	"github.com/go-git/go-git/v5"
	"github.com/jessevdk/go-flags"
)

type InitCommand struct {
	Bare bool `long:"bare" description:"Create a bare repository." required:"true"`
	Args struct {
		Directory string `positional-arg-name:"directory" description:"The directory initialize."`
	} `positional-args:"yes"`
}

var initCommand InitCommand

func (x *InitCommand) Execute(a []string) error {
	if len(a) != 0 {
		return &flags.Error{flags.ErrDuplicatedFlag, "only one directory can be specified"}
	}

	dir := options.GitDir
	if x.Args.Directory != "" {
		dir = x.Args.Directory
	}

	log := slog.With("directory", dir)
	log.Debug("init")
	_, err := git.PlainInitWithOptions(dir, &git.PlainInitOptions{
		Bare: x.Bare,
	})
	if err == git.ErrRepositoryAlreadyExists {
		fmt.Printf("Reinitialized existing Git repository in %s\n", dir)
		return nil
	}
	if err != nil {
		return err
	}
	fmt.Printf("Initialized empty Git repository in %s\n", dir)

	return nil
}

func init() {
	parser.AddCommand("init",
		"Create an empty bare Git repository.",
		`This command creates an empty bare Git repository to be able to fetch objects.

     If the $GIT_DIR environment variable is set then it specifies a path to use.`,
		&initCommand)
}
