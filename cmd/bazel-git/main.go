package main

import (
	"fmt"
	"log/slog"
	"os"

	"github.com/jessevdk/go-flags"
)

type Options struct {
	GitDir   string       `long:"git-dir" description:"Path to store objects." value-name:"<path>" env:"GIT_DIR" default:".git"`
	WorkTree string       `long:"work-tree" description:"Path to the working tree." value-name:"<path>" env:"GIT_WORK_TREE" default:"."`
	LogLevel func(string) `long:"log-level" description:"Set the structured logging level." choice:"debug" choice:"info" choice:"warning" choice:"error" default:"error"`
}

var options Options

var parser = flags.NewParser(&options, flags.HelpFlag|flags.PassDoubleDash)

func main() {
	var level = new(slog.LevelVar)

	handler := slog.NewJSONHandler(os.Stderr, &slog.HandlerOptions{Level: level})

	slog.SetDefault(slog.New(handler))

	options.LogLevel = func(v string) {
		switch v {
		case "debug":
			level.Set(slog.LevelDebug)
		case "info":
			level.Set(slog.LevelInfo)
		case "warning":
			level.Set(slog.LevelWarn)
		case "error":
			level.Set(slog.LevelError)
		default:
			panic("unreachable: error level")
		}
	}

	if _, err := parser.Parse(); err != nil {
		switch e := err.(type) {
		case *ExitCodeError:
			if e.Error() != "" {
				fmt.Fprintf(os.Stderr, "fatal: %s\n", e)
			}
			os.Exit(e.ExitCode)
		case *flags.Error:
			switch e.Type {
			case flags.ErrHelp:
				fmt.Fprint(os.Stdout, e)
				os.Exit(0)
			default:
				fmt.Fprintf(os.Stderr, "fatal: %s\n", e)
				os.Exit(2)
			}
		default:
			fmt.Fprintf(os.Stderr, "fatal: %s\n", e)
			os.Exit(1)
		}
	}
}
