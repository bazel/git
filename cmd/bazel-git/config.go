package main

import (
	"errors"
	"fmt"
	"log/slog"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/jessevdk/go-flags"
)

type ConfigCommand struct {
	Args struct {
		Name  string `positional-arg-name:"<name>" description:"The configuration option name." required:"yes"`
		Value string `positional-arg-name:"<value>" description:"The configuration option value." required:"yes"`
	} `positional-args:"yes"`
}

var configCommand ConfigCommand

func (x *ConfigCommand) Execute(rest []string) error {
	if len(rest) != 0 {
		return &flags.Error{flags.ErrDuplicatedFlag, "invalid number of positional arguments"}
	}

	log := slog.With("cmd", "config", "name", x.Args.Name, "value", x.Args.Value)

	log.Debug("open", "git-dir", options.GitDir)
	repo, err := git.PlainOpen(options.GitDir)
	if err != nil {
		log.Error("open", "err", err)
		return err
	}

	log.Debug("config")
	config, err := repo.Config()
	if err != nil {
		log.Error("config", "err", err)
		return err
	}

	if config.Raw == nil {
		return errors.New("Unexpected empty raw configuration value")
	}

	parts := strings.Split(x.Args.Name, ".")
	log.Debug("split", "parts", parts)
	if len(parts) != 2 {
		return fmt.Errorf("Invalid configuration value name: %s", x.Args.Name)
	}

	section, name := parts[0], parts[1]

	log.Debug("get", "section", section)
	if !config.Raw.HasSection(section) {
		return fmt.Errorf("`%s` section not found: %s", x.Args.Name, section)
	}
	s := config.Raw.Section(section)

	s.SetOption(name, x.Args.Value)

	log.Debug("set")
	return repo.SetConfig(config)
}

func init() {
	parser.AddCommand("config",
		"Set repository options.",
		`Sets repository options using a dot-separated key and value pair.`,
		&configCommand)
}
