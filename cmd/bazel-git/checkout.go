package main

import (
	"log/slog"

	"github.com/go-git/go-billy/v5/osfs"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/cache"
	"github.com/go-git/go-git/v5/storage/filesystem"
	"github.com/jessevdk/go-flags"
)

type CheckoutCommand struct {
	Force bool `short:"f" long:"force" description:"When switching branches, proceed even if the index or the working tree differs from **HEAD**, and even if there are untracked files in the way. This is used to throw away local changes and any untracked files or directories that are in the way."`
	Args  struct {
		Revision plumbing.Revision `positional-arg-name:"<revision>" description:"The revision to update the working tree to."`
	} `positional-args:"yes" required:"yes"`
}

var checkoutCommand CheckoutCommand

func (x *CheckoutCommand) Execute(rest []string) error {
	if len(rest) != 0 {
		return &flags.Error{flags.ErrDuplicatedFlag, "invalid number of positional arguments"}
	}

	log := slog.With("cmd", "checkout", "revision", x.Args.Revision)

	log.Debug("open", "git-dir", options.GitDir)
	storage := filesystem.NewStorage(osfs.New(options.GitDir), cache.NewObjectLRUDefault())
	working := osfs.New(options.WorkTree)
	repo, err := git.Open(storage, working)
	if err != nil {
		log.Error("open", "err", err)
		return err
	}

	log.Debug("resolve")
	hash, err := repo.ResolveRevision(x.Args.Revision)
	if err != nil {
		log.Error("resolve", "err", err)
		return err
	}

	log.Debug("worktree", "work-tree", options.WorkTree)
	worktree, err := repo.Worktree()
	if err != nil {
		log.Error("worktree", "err", err)
		return err
	}

	log.Debug("checkout", "hash", hash)
	err = worktree.Checkout(&git.CheckoutOptions{
		Hash:  *hash,
		Force: x.Force,
	})
	if err != nil {
		log.Error("checkout", "err", err)
		return err
	}

	return err
}

func init() {
	parser.AddCommand("checkout",
		"Update the working tree to a commit.",
		`Updates files in the working tree to match the commit in the index.`,
		&checkoutCommand)
}
