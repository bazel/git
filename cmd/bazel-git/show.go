package main

import (
	"fmt"
	"io"
	"log/slog"
	"os"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/utils/ioutil"
	"github.com/jessevdk/go-flags"
)

type ShowCommand struct {
	Args struct {
		Revision plumbing.Revision `positional-arg-name:"<object>" description:"The object to show."`
	} `positional-args:"yes" required:"yes"`
}

var showCommand ShowCommand

func (x *ShowCommand) Execute(rest []string) error {
	if len(rest) != 0 {
		return &flags.Error{flags.ErrDuplicatedFlag, "invalid number of positional arguments"}
	}

	log := slog.With("cmd", "show", "object", x.Args.Revision)

	log.Debug("open", "git-dir", options.GitDir)
	repo, err := git.PlainOpen(options.GitDir)
	if err != nil {
		log.Error("open", "err", err)
		return err
	}

	log.Debug("resolve")
	hash, err := repo.ResolveRevision(x.Args.Revision)
	if err != nil {
		log.Error("resolve", "err", err)
		return err
	}
	log.Debug("resolved", "hash", hash)

	if !strings.Contains(x.Args.Revision.String(), ":") {
		return fmt.Errorf("Only blobs with filepaths are supported (`<commit>:<filepath>`) : %s", x.Args.Revision.String())
	}

	log.Debug("commit")
	commit, err := repo.CommitObject(*hash)
	if err != nil {
		log.Error("commit", "err", err)
		return err
	}

	log.Debug("tree")
	tree, err := commit.Tree()
	if err != nil {
		log.Error("tree", "err", err)
		return err
	}

	filepath := strings.SplitN(x.Args.Revision.String(), ":", 2)[1]

	log.Debug("file", "filepath", filepath)
	file, err := tree.File(filepath)
	if err != nil {
		log.Error("tree", "err", err)
		return err
	}

	log.Debug("reader")
	reader, err := file.Reader()
	if err != nil {
		log.Error("reader", "err", err)
		return err
	}
	defer ioutil.CheckClose(reader, &err)

	log.Debug("copy")
	_, err = io.Copy(os.Stdout, reader)
	if err != nil {
		log.Error("copy", "err", err)
		return err
	}

	return err
}

func init() {
	parser.AddCommand("show",
		"Show various types of objects.",
		`Shows one or more objects (blobs, tree, tags and commits).

 For commits it shows the log message and textual diff. It also presents the merge
 commit in a special format as produced by 'git diff-tree --cc'.

 For tags, it shows the tag message and the referenced objects.

 For trees, it shows the names (equivalent to 'git ls-tree' with '--name-only').

 For plain blobs, it shows the plain contents.`,
		&showCommand)
}
