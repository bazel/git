# Testing

Each `sh_test` tests the CLI for various conformance.

By default, the `//cmd/bazel-git` is tested.

To test the `git` on `PATH`, run:

```console
$ bazelik test ... --//test:git=@git
```

This allows comparison to other CLI implementations.
