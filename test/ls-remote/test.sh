#! /usr/bin/env sh

# Strict Shell
set -o errexit
set -o nounset

# Grab the `git` CLI
test "${1}" = "--git"
GIT="${2}"
shift 2

printf 'Testing: %s\n' "${*}"

# Validate `git` is executable
if ! test -x "${GIT}"; then
  printf >&2 '%s: not executable\n' "${GIT}"
  exit 1
fi

# Initialize a directory
if ! "${GIT}" init --bare .git; then
  printf >&2 '%s: failed to initialise\n' "${GIT}"
  exit 1
fi

# List the remote
if ! "${GIT}" --git-dir .git ls-remote "${@}"; then
  printf >&2 '%s: failed to ls-remote\n' "${GIT}"
  exit 1
fi

printf 'Success %s\n' "${*}"
