#! /usr/bin/env sh

# Strict Shell
set -o errexit
set -o nounset

# Grab the `git` CLI
test "${1}" = "--git"
GIT="${2}"
shift 2

printf 'Testing: %s\n' "${*}"

# Validate `git` is executable
if ! test -x "${GIT}"; then
  printf >&2 '%s: not executable\n' "${GIT}"
  exit 1
fi

# Initialize a directory
if ! "${GIT}" init --bare .git; then
  printf >&2 '%s: failed to initialise\n' "${GIT}"
  exit 1
fi

# Fetch
if ! "${GIT}" --git-dir .git fetch --depth 1 https://git.gitlab.arm.com/bazel/git.git refs/tags/v1.0.0-alpha.10; then
  printf >&2 '%s: failed to initialise\n' "${GIT}"
  exit 1
fi

# List the tree
if ! "${GIT}" --git-dir .git ls-tree "${@}"; then
  printf >&2 '%s: failed to ls-tree\n' "${GIT}"
  exit 1
fi

printf 'Success: %s\n' "${*}"
