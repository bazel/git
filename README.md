# `bazel-git`

> A hermetic `git` implementation to provide an extreme subset of upstream `git` CLI functionality to enable hermetic `git` operations as part of `rules_git`.

## Getting Started

Install with `go`, assuming `GOPATH` (`~/go/bin`) is on `PATH`:

```sh
go install gitlab.arm.com/bazel/git@v1
```

Run one of the commands:

```console
$ bazel-git init --bare /tmp/something
$ bazel-git config core.compression 9
$ bazel-git ls-remote --exit-code https://github.com/git/git.git refs/tags/v1.7.8
$ bazel-git \
  --git-dir /tmp/something \
  fetch \
  --depth 1 \
  --no-write-fetch-head \
  https://github.com/git/git.git \
  refs/tags/v1.7.8
$ bazel-git \
  --git-dir /tmp/something
  cat-file \
  -e \
  8b891d2711bdfb153176cc325150e0233bdff175
$ bazel-git \
  --git-dir /tmp/something \
  --work-tree /tmp/else \
  checkout \
  8b891d2711bdfb153176cc325150e0233bdff175
$ head -n5 /tmp/else/README.md
////////////////////////////////////////////////////////////////

        GIT - the stupid content tracker

////////////////////////////////////////////////////////////////
```

## Usage

The binary implements a fully hermetic way to retrieve `git` repositories over the network.

It implements an _extreme_ subset of the upstream `git` CLI.

The goal is to allow the bazel/rules_git> bindings to hermetically retrieve code via `git` protocols.
